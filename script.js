//declaring the variables.
let bankBalance = 0;
let payBalance = 0;
let loanBalance = 0;
let activeLoan = false;
let laptops = [];
const baseUrl = "https://hickory-quilled-actress.glitch.me/";

//declaring HTML-elements.
const balanceElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const loanElement = document.getElementById("loanBalance");
const featuresElement = document.getElementById("features");
const laptopsElement = document.getElementById("laptopSelector");
const lapTopTitle = document.getElementById("lapTopTitle");
const lapTopDescription = document.getElementById("lapTopDescription");
const lapTopPrice = document.getElementById("lapTopPrice");
const lapTopFeatures = document.getElementById("lapTopFeatures");
const loanButton = document.getElementById("loanButton");
const bankButton = document.getElementById("bankButton");
const workButton = document.getElementById("workButton");
const buyButton = document.getElementById("buyButton");
const repayButton = document.getElementById("repayButton");
const lapTopImage = document.getElementById("lapTopImage");
const outstandingLoan = document.getElementById("outstandingLoan");
const secondSection = document.getElementById("second-section");

/*
  This method fetches all the computers from API
 */
const getComputers = async () => {
  try {
    const response = await fetch(baseUrl + "computers");
    const post = await response.json();
    laptops = post;
    addLaptopsToList(laptops);
  } catch (error) {
    console.error(error);
  }
};

//initializing all required values
getComputers();
outstandingLoan.style.visibility = "hidden";
repayButton.style.visibility = "hidden";
secondSection.style.visibility = "hidden";
lapTopFeatures.style.visibility = "hidden";
balanceElement.innerText = bankBalance;
payElement.innerText = payBalance;

/*
  This method adds all fetched laptops to the dropdown menu.
 */
const addLaptopsToList = (laptops) =>
  laptops.forEach((x) => addLaptopToList(x));

/*
  This method add the specific laptop to the dropdown menu.
 */
const addLaptopToList = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

/*
  This method handles interaction when user changes the selected laptop from the dropdown menu.
 */
const handleLaptopSelectChange = () => {
  secondSection.style.visibility = "visible";
  lapTopFeatures.style.visibility = "visible";
  const selectedLaptop = laptops[laptopsElement.selectedIndex - 1];
  lapTopPrice.innerText = selectedLaptop.price;
  lapTopTitle.innerText = selectedLaptop.title;
  lapTopDescription.innerText = selectedLaptop.description;
  lapTopImage.src = baseUrl.concat(selectedLaptop.image);
  replaceFeatures(selectedLaptop.specs);
};

/*
  This method updated the features UI-element for the selected laptop.
 */
const replaceFeatures = (features) => {
  featuresElement.replaceChildren();
  features.forEach((x) => {
    const node = document.createElement("li");
    node.appendChild(document.createTextNode(x));
    featuresElement.appendChild(node);
  });
};

/*
  This method adds 100 to the payBalance when user clicks on the Work button.
 */
const doWork = () => {
  payBalance += 100;
  payElement.innerText = payBalance;
};

/*
  This method transfers the payBalance to bankBalance.
  Checks if the users has an active loan, if then
  10% of the payBalance is deducted and paid on the loan.
  The rest is added to bankBalance.
  If not: total payBalance is transferred to bankBalance.
 */
const transferToBank = () => {
  if (activeLoan) {
    const amountToLoanBalance = payBalance * 0.1;
    if (loanBalance <= amountToLoanBalance) {
      loanPaidBack(amountToLoanBalance);
    } else {
      loanBalance -= amountToLoanBalance;
      loanElement.innerText = loanBalance;
    }
    bankBalance += payBalance - amountToLoanBalance;
  } else {
    bankBalance += payBalance;
  }
  payBalance = 0;
  payElement.innerText = payBalance;
  balanceElement.innerText = bankBalance;
};

/**
  This method handles the actions required when user clicks
   on the Get loan button. Check if the user already has an active
   loan. Then checks if the amount entered is valid and not greater than
   2 times the bankBalance.
 */
const handleGetLoan = () => {
  if (activeLoan) {
    alert("You can only have one loan. Repay your current loan first");
    return;
  }

  const wantedLoan = prompt("Please enter the amount you wish to loan").trim();

  if (wantedLoan == null) return; //user cancelled

  if (isNaN(wantedLoan) || wantedLoan == "" || parseInt(wantedLoan) < 1) {
    alert("Incorrect input. Must be a positive number. Please try again");
    return;
  }

  if (parseInt(wantedLoan) > bankBalance * 2) {
    alert("You cannot loan more than double of your bank balance!");
    return;
  }

  giveLoan(wantedLoan);
};

/*
  Get the user a loan. Enable info about
  loan in bank and repay  button in Work section.
 */
const giveLoan = (wantedLoan) => {
  alert("Congratulations! You got a loan!");
  loanBalance = parseInt(wantedLoan);
  loanElement.innerText = loanBalance;
  bankBalance += loanBalance;
  balanceElement.innerText = bankBalance;
  activeLoan = true;
  outstandingLoan.style.visibility = "visible";
  repayButton.style.visibility = "visible";
};

/**
  This method handles the actions required when user clicks
  on the repay button. Handles if the loan will be
  paid back in total or not.
 */
const handleRepay = () => {
  if (loanBalance <= payBalance) {
    loanPaidBack(payBalance);
  } else {
    loanBalance -= payBalance;
    loanElement.innerText = loanBalance;
  }
  payBalance = 0;
  payElement.innerText = payBalance;
};

/**
  This method is called when the loan is going to be paid back
  during next transaction. Gives user feedback and resets all
  parameters for loan. Disabling loan info in bank and repay
  button.
 
 */
const loanPaidBack = (payBack) => {
  const toBankBalance = payBack - loanBalance;
  loanBalance = 0;
  (loanElement.innerText = 0), (bankBalance += toBankBalance);
  balanceElement.innerText = bankBalance;
  outstandingLoan.style.visibility = "hidden";
  repayButton.style.visibility = "hidden";
  activeLoan = false;
  alert("Congratulations! You have now repaid your loan!");
};

/**
  This method handles required actions when user clicks on
  the Buy button. Check whether user can buy the laptop or not.
 */
const handleBuyLaptop = () => {
  const selectedLaptop = laptops[laptopsElement.selectedIndex - 1];
  if (bankBalance >= parseInt(selectedLaptop.price)) {
    alert(
      "Congratulations! You are now the owner of a " +
        `new laptop: ${selectedLaptop.title}. Enjoy!`
    );
    bankBalance -= parseInt(selectedLaptop.price);
    balanceElement.innerText = bankBalance;
  } else {
    alert("Sorry, you cannot afford the laptop at the moment.");
  }
};

//add  event-listeners
workButton.addEventListener("click", doWork);
bankButton.addEventListener("click", transferToBank);
laptopsElement.addEventListener("change", handleLaptopSelectChange);
loanButton.addEventListener("click", handleGetLoan);
buyButton.addEventListener("click", handleBuyLaptop);
repayButton.addEventListener("click", handleRepay);

# Komputer Store

### About

A dynamic webpage using “vanilla” JavaScript,the guidelines given below.
The Bank – an area where you will store funds and make bank loans
Work – an area to increase your earnings and deposit cash into your bank balance
Laptops – an area to select and display information about the merchandise

## Install

Clone repository

## Usage

Use the Live Server Visual Studio Code plugin to open the website with live server.

## Contributing

- [Kamperopoulos Anastasios](https://gitlab.com/KamperopoulosA)

## Built with

- Visual Studio Code
- Html
- Css
- Javascript
